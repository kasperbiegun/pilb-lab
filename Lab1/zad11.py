a = [1, 2, 12, 4]
b = [2, 4, 2, 8]

if len(a) == len(b):
    dot_product = 0
    for i in range(len(a)):
        dot_product += (a[i]*b[i])
    print(dot_product)
else:
    print("Not the same length")