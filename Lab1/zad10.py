fin = open("random_text.txt", "r")
fout = open("replace.txt", "w+")
replace_words = {
    'i': 'oraz',
    'oraz': 'i',
    'nigdy': 'prawie nigdy',
    'dlaczego': 'czemu'
}
for line in fin:
    for word in replace_words:
        line = line.replace(word, replace_words[word])
    fout.write(line)
fin.close()
fout.close()
