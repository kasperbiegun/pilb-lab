from random import randint
numbers = []


def sort_descending(list):
    for iter_num in range(len(list) - 1, 0, -1):
        for index in range(iter_num):
            if list[index] < list[index + 1]:
                temp = list[index]
                list[index] = list[index + 1]
                list[index + 1] = temp
    return(list)


for _ in range(50):
    numbers.append(randint(0, 1000))

numbers1 = sort_descending(numbers)
numbers.sort(reverse=True)

print(numbers1)
print(numbers)