import numpy

size = numpy.random.randint(1, 11)

matrix = numpy.random.random((size,size))
det = numpy.linalg.det(matrix)

print(det)