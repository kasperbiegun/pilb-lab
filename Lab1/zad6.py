import os

#os.rename('*.jpg', '*.png')
fnames = os.listdir('.')
for file in fnames:
    if '.jpg' in file:
        name = file.split('.')[0]
        new_fname = name + '.png'
        os.rename(file, new_fname)
