import os

for root, dirs, files in os.walk('.'):
    print('\t/' + os.path.basename(root))
    for f in files:
        print('\t\t' + f)