import numpy

matrix_A = numpy.random.random((8,8))
matrix_B = numpy.random.random((8,8))

result = numpy.matmul(matrix_A, matrix_B)

print(result)