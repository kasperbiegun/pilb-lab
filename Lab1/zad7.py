from math import sqrt

a, b, c = input("Enter a,b,c (separated by comma): ").split(',')
a = int(a)
b = int(b)
c = int(c)

delta = b**2 - 4*a*c
if delta > 0:
    print("x1 = " + str((-b-sqrt(delta))/(2*a)))
    print("x2 = " + str((-b+sqrt(delta))/(2*a)))
elif delta == 0:
    print("x0 = " + str((-b/(2*a))))
else:
    print("SORRY BRO, delta = " + str(delta))
