import random
from threading import Thread

MIN = 0
MAX = 10
N = 5
N_BUCKETS = 10

data = [random.uniform(MIN, MAX) for i in range(N)]
threads = []
buckets = {}


def create_counter(bucket_num, start_val, stop_val):
    def count():
        buckets[bucket_num] = sum(1 for d in data if start_val <= d < stop_val)
    return count


width = (MAX - MIN) / N_BUCKETS
for i in range(N_BUCKETS):
    threads.append(Thread(target=create_counter(i, i * width, (i + 1) * width)))

for thread in threads:
    thread.start()

for thread in threads:
    thread.join()

print('Data: {}'.format(data))
print('Buckets: {}'.format(buckets))
