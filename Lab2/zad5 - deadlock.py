import time
import threading


class Philosopher(threading.Thread):
    def __init__(self, name, lFork, rFork):
        threading.Thread.__init__(self)
        self.name = name
        self.lFork = lFork
        self.rFork = rFork
        self.enter()
        return

    def run(self):
        self.pickLeftFork()
        self.pickRightFork()
        self.eat()

    def enter(self):
        print(self.name + " enters the dining room")
        return

    def pickLeftFork(self):
        self.lFork.acquire()
        print(self.name + " picks up left fork")
        time.sleep(1);
        return

    def pickRightFork(self):
        self.rFork.acquire()
        print(self.name + " picks up right fork")
        time.sleep(1);
        return

    def eat(self):
        print(self.name + " is eating")
        time.sleep(3);
        print(self.name + " finished eating")
        self.lFork.release()
        self.rFork.release()
        return


fork1 = threading.Lock()
fork2 = threading.Lock()
fork3 = threading.Lock()
fork4 = threading.Lock()
fork5 = threading.Lock()

socrates = Philosopher("Socrates", fork1, fork2);
plato = Philosopher("Plato", fork2, fork3);
aristotle = Philosopher("Aristotle", fork3, fork4);
confucius = Philosopher("Confucius", fork4, fork5);
voltaire = Philosopher("Voltaire", fork5, fork1);

philosophers = [socrates, plato, aristotle, confucius, voltaire]

for philosopher in philosophers:
    philosopher.start()

for philosopher in philosophers:
    philosopher.join()
print("Every philosopher has finished eating")