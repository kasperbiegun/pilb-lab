import time
import threading


class Philosopher(threading.Thread):
    def __init__(self, name, lFork, rFork, waiter):
        threading.Thread.__init__(self)
        self.name = name
        self.lFork = lFork
        self.rFork = rFork
        self.waiter = waiter
        self.enter()
        return

    def run(self):
        self.pickForks()
        self.eat()

    def enter(self):
        print(self.name + " enters the dining room")
        return

    def pickForks(self):
        self.waiter.getForks(self)

    def pickLeftFork(self):
        self.lFork.acquire()
        print(self.name + " picks up left fork")
        time.sleep(1);
        return

    def pickRightFork(self):
        self.rFork.acquire()
        print(self.name + " picks up right fork")
        time.sleep(1);
        return

    def eat(self):
        print(self.name + " is eating")
        time.sleep(3);
        print(self.name + " finished eating")
        self.lFork.release()
        self.rFork.release()
        return


class Waiter:
    def __init__(self):
        self.attention = threading.Lock()

    def getForks(self, philosopher):
        print("Waiter is called by " + philosopher.name)
        self.attention.acquire()
        print("Waiter brings forks for " + philosopher.name)
        philosopher.pickLeftFork()
        philosopher.pickRightFork()
        print("Waiter brought forks for " + philosopher.name)
        self.attention.release()
        return


waiter = Waiter();

fork1 = threading.Lock()
fork2 = threading.Lock()
fork3 = threading.Lock()
fork4 = threading.Lock()
fork5 = threading.Lock()

socrates = Philosopher("Socrates", fork1, fork2, waiter);
plato = Philosopher("Plato", fork2, fork3, waiter);
aristotle = Philosopher("Aristotle", fork3, fork4, waiter);
confucius = Philosopher("Confucius", fork4, fork5, waiter);
voltaire = Philosopher("Voltaire", fork5, fork1, waiter);

philosophers = [socrates, plato, aristotle, confucius, voltaire]

for philosopher in philosophers:
    philosopher.start()

for philosopher in philosophers:
    philosopher.join()
print("Every philosopher has finished eating")

