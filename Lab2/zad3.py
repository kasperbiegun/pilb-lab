import xml.dom.minidom
import xml.sax
from xml.dom.minidom import parse

# SAX PART
class AddressBookHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.CurrentData = ""
        self.phone = ""
        self.address = ""

    def startElement(self, tag, attributes):
        self.CurrentData = tag
        if tag == "person":
            print("=== Address Book Entry")
            name = attributes["name"]
            print("Name:", name)

    def endElement(self, tag):
        if self.CurrentData == "phone":
            print("Phone Number:", self.phone)
        elif self.CurrentData == "address":
            print("Postal Address:", self.address)
        self.CurrentData = ""

    def characters(self, content):
        if self.CurrentData == "phone":
            self.phone = content
        elif self.CurrentData == "address":
            self.address = content


if __name__ == "__main__":
    parser = xml.sax.make_parser()
    parser.setFeature(xml.sax.handler.feature_namespaces, 0)
    Handler = AddressBookHandler()
    parser.setContentHandler(Handler)

    print("SAX PART\n")
    parser.parse("example.xml")

    print("\n\nDOM PART\n")
    # DOM PART
    DOMTree = xml.dom.minidom.parse("example.xml")
    collection = DOMTree.documentElement
    if collection.hasAttribute("shelf"):
        print("Root element : %s" % collection.getAttribute("shelf"))

    # Get all the movies in the collection
    people = collection.getElementsByTagName("person")

    # Print detail of each movie.
    for person in people:
        print("=== Address Book Entry")
        if person.hasAttribute("name"):
            print("Name: %s" % person.getAttribute("name"))

        type = person.getElementsByTagName('phone')[0]
        print("Phone: %s" % type.childNodes[0].data)
        format = person.getElementsByTagName('address')[0]
        print("Postal Address: %s" % format.childNodes[0].data)
