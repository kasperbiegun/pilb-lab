class Complex(object):
    def __init__(self, r, i):
        self.real = r
        self.imaginary = i

    def __add__(self, other):
        return Complex(self.real + other.real, self.imaginary + other.imaginary)

    def __sub__(self, other):
        return Complex(self.real - other.real, self.imaginary - other.imaginary)

    def __mul__(self, other):
        return Complex(self.real * other.real - self.imaginary * other.imaginary,
                       self.imaginary * other.real + self.real * other.imaginary)

    def __abs__(self):
        return sqrt(self.real ** 2 + self.imaginary ** 2)

    def __str__(self):
        return '({}, {})'.format(self.real, self.imaginary)


A = Complex(1, -2)
B = Complex(3, 4)

C = A + B
D = A - B
E = A * B
F = Complex.__str__(A)

print(C)
print(D)
print(E)
print(F)